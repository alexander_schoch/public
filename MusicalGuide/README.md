# A Guide through my Taste in Music

Ich werde gelegentlich gefragt, welche Genres oder Interpreten ich eigentlich so höre. Da es sich dabei um ein Gebiet handelt, welches prinzipiell Keiner kennt. Insofern ist dies mein Versuch, dich so kurz und detailliert wie möglich in die riesige Welt des (grösstenteils) functional Jazz einzuführen.

## How to Use

Unter "Beispiele" findest du die Vorschläge (Link im jeweiligen Titel), welche ich herausgesucht habe. Dabei wurde Wert darauf gelegt, dass das Stück representativ für die dazugehörende Beschreibung ist und gleichzeitig natürlich musikalisch ansprechend ist. Ausserdem versuche ich, dir den Weg durch diesen Guide durch die Wahl der Reihenfolge so einfach wie möglich zu gestalten. Gelegentlich verweise ich auch auf weitere Stücke/Variationen, welche in der Beschreibung verlinkt werden. Diese können bei Interesse angehört werden, gehören aber nicht zum "offiziellen Programm".

Der Guide ist so konzipiert, dass du zuerst auf den Link klickst (alle Links führen nach YouTube), das Stück im Hintergrund abspielst und dabei die Beschreibung liest. Versuche, die beschriebenen Konzepte herauszuhören und zu verstehen.

## Über diesen Guide

Bitte beachte, dass mein Musikstil absolut nicht nur aus Jazz besteht, sondern auch ganz viele andere Genres beinhält. Allerdings sind diese generell viel bekannter (ich will dir hier ja etwas Neues zeigen) und in meinem Fall weniger konsistent.

Ausserdem sei noch gesagt, dass es sehr, sehr viele Untergenres von Jazz gibt (e.g. Bebop, Cool Jazz, etc.). Allerdings verschmelzen diese meist so stark, dass es absolut keinen Sinn ergibt, die Beispiele zu unterteilen (und es benutzt die Bezeichnungen auch keiner). Insofern ist es mir an dieser Stelle wichtiger, typische Konzepte des Jazz zu erfassen (Siehe nächsten Abschnitt).

## Beschreibungen und Definitionen

| Thema | Beschreibung |
| ----: | :---------- |
| Jazz  | Das Genre "Jazz" ist absolut riesig. Allerdings gibt es einige Konzepte, welche immer wieder auftreten uns sehr typisch sind. Einerseits ist dies die Strutkur: Sehr oft wird ein Thema eingeführt, nach welchem ein grundsätztlich improvisierter Soloteil folgt und das Stück dann wieder mit dem Thema abgeschlossen wird. Ausserdem sind viele Jazz-Stücke im Swing geschrieben (geschwungene Achtel). Ebenfalls ganz typisch ist die "functional Harmony", was beschreibt, dass die vielen und kopmlizierten Akkorde ("Changes"), welche dem Stück (und vor allem auch dem Soloteil) das Fundament liefern, harmonietechnisch logisch sind. Ebenfalls sehr typisch ist der so genannte "Walking Bass", bei welchem der Bass auf jeden Viertel einen anderen Ton spielt und so die Verbindung zwischen Akkorden aufrechterhält (gut hörbar in [James Moody - Disappointed](https://www.youtube.com/watch?v=9d_q3lfOExQ)). |
| Jazz Standard | Ein sehr grosser Teil des Jazz sind so genannte "Standards". Das sind Stücke, welche irgendwann mal geschrieben wurden und dann immer wieder in neuen Versionen reinterpretiert werden. Infolgedessen ist es sehr schwierig, einen Standard einem Komponisten zuzuschreiben. Interessanterweise können auch Stücke/Songs zu Standards werden, e.g. [Someday my Prince Will Come](https://www.youtube.com/watch?v=EaCzgfIPmsk) von Disney's "Schneewittchen und die sieben Zwerge" oder [All about the Base](https://www.youtube.com/watch?v=iyTTX6Wlf1Y) von Meghan Trainor. |


## Beispiele

### [Pat Metheny - Phase Dance](https://www.youtube.com/watch?v=tdyWOl-5G9o)

| | |
| ---: | :--- |
| Interpret | Pat Metheny Group |
| Album | Pat Metheny Group | 
| Name | Phase Dance |
| Jahr | 1978 |

Beginnen wir mit etwas Leichtem.

"Phase Dance" ist wohl eines der bekanntesten Stücke der Pat Metheny Group. Ihr Stil zeichnet sich durch kristallklaren Gitarrensound (Pat Metheny selbst) und sehr Jazzigen Changes aus, wobei die Group Members sehr gerne auch unisono mit der Melodie "mitsingen" (Beispiel: [Pat Metheny Group - 5-5-7](https://www.youtube.com/watch?v=OQv4JGnXei4)). Sehr typisch (eher in neuen Albem, e.g. "Secret Story") ist auch der sehr ruhige Aufbau auf ein wundervolles Synthesizer-Solo, welches dadurch noch viel mehr Power erhält (Beispiel: [Pat Metheny Group - The Truth will Always Be](https://www.youtube.com/watch?v=2DxfuGI_DaM)). Metheny-Stücke sind prinzpipiell keine Standards.

Obwohl Der stil dieser Musik technisch gesehen schon irgendwie "Jazz" ist, klingt es absolut nicht typisch. Das liegt daran, dass das Thema-Solo-Thema-Schema in diesem Beispiel nicht wirklich klar ersichtlich ist, fast alle Metheny-Stücke binär (kein Swing) sind und keinen typschen Walking Bass haben.

Ich persönlich mag Pat Metheny sehr, um zu lernen (konstantes Tempo, nicht zu aufregend, kein Text, trotzdem wundervoll), zum Autofahren und am morgen im Zug etwas Ruhe geniessen zu können.

### [Sammy Nestico - Switch in Time](https://www.youtube.com/watch?v=Kwmg47jQhMc)

| | |
| ---: | :--- |
| Interpret | Count Basie |
| Komponist | Sammy Nestico |
| Album | Basie Straight Ahead |
| Name | Switch in Time |
| Jahr | 1968 |
