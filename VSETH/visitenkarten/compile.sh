#!/bin/bash

# HOW TO USE
# Write all the data for the cards into data.csv, in the following format:
# name,role,room,telephone,email
# Alexander Schoch,Internal Affairs,E 23,+41 44 632 75 28,alexander.schoch@vseth.ethz.ch

while read line
do
  name=$( echo $line | cut -d ',' -f 1 )
  firstname=$( echo $name | cut -d ' ' -f 1 )
  lastname=$( echo $name | cut -d ' ' -f 2 )
  role=$( echo $line | cut -d ',' -f 2 )
  room=$( echo $line | cut -d ',' -f 3 | sed 's/ /\\\\,/g' )
  telplain=$( echo $line | cut -d ',' -f 4  | sed 's/ //g' )
  tel=$( echo $line | cut -d ',' -f 4  | sed 's/ /\\\\,/g' )
  mail=$( echo $line | cut -d ',' -f 5 )
  vcard=$( echo $line | cut -d ',' -f 6 )
  filename=$( echo $name | sed 's/ /_/g' )

  [[ $1 == '--only' ]] && [[ $name != "$2" ]] && continue

  echo "Generating Business Card for: $firstname $lastname"
  echo "  Generating vcard..."
  cat tex/vcard.txt | sed "s/vsethnachname/$lastname/g; s/vsethvorname/$firstname/g; s/vsethname/$name/g; s/vsethemail/$mail/g; s/vsethtel/$telplain/g; s/vsethrole/$role/g; s/vsethroom/$room/g" | qrencode -s 50 -o vcards/$filename.png

  echo "  Recoloring vcard..."
  convert vcards/$filename.png -transparent "#ffffff" xc:"#009fe3" -channel RGB -clut -trim vcards/$filename.png

  echo "  Compiling Business Card..."
  cat tex/visitenkarte.tex | sed "s/vsethname/$name/g; s/vsethrole/$role/g; s/vsethroom/$room/g; s/vsethtel/$tel/g; s/vsethmail/$mail/g; s/vsethvcard/$filename.png/g;" > $filename.tex
  lualatex $filename.tex > /dev/null
  mv $filename.pdf output
  rm $filename.*
done < data.csv
