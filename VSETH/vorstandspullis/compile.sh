#!/bin/bash

# HOW TO USE
# Write all the data for the cards into data.csv, in the following format:
# name,role,room,telephone,email
# Alexander Schoch,Internal Affairs,E 23,+41 44 632 75 28,alexander.schoch@vseth.ethz.ch

while read line
do
  [[ -z $line ]] && continue

  nickname=$( echo $line | cut -d ',' -f 1 )
  name=$( echo $line | cut -d ',' -f 2 )
  role=$( echo $line | cut -d ',' -f 3 )
  size=$( echo $line | cut -d ',' -f 4 )
  filename=${size}_$( echo $name | sed 's/ /_/g' )

  [[ $1 == '--only' ]] && [[ $name != "$2" ]] && continue

  echo "Generating Vorstandspulli for: $name"
  echo "  Generating latex file..."
  cat tex/pullis.tex | sed "s/vsethname/$nickname/g; s/vsethrole/$role/g; " > $filename.tex

  echo "  Compiling Vorstandspulli..."
  lualatex $filename.tex > /dev/null

  echo "  Generating SVG..."
  dvisvgm --pdf $filename.pdf &> /dev/null
  mv $filename.svg output
  #rm $filename.*
done < data.csv
