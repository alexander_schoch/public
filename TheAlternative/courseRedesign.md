---
title: Course Redesign
author: Alexander Schoch
date: \today

header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}

numbersections: true
---

\maketitle

\tableofcontents

# Courses to work on 

- **Intro course**: Rework such that it fits the description of the course
- **The power of linux, the linux toolkit and hacker session**: merge into two *console courses*, which don't contain the boring stuff from toolkit.


# Introduction to Linux

Description on `www.thealternative.com`:

"Now that you know the ethical advantages of Free Software, learn about the structure of Linux, its most important concepts and the comfort accompanying them. Discover different flavours of Linux and get a feeling which one would be for you."

## Contents of the "new" course

This list just takes the description written above and reformulates it into content.

- "Structure of Linux" $\rightarrow$ init systems, Display Managers, File System (but only minimally), Kernel, etc. For the intro course, it should be enough to mention the kernel and the file system, as init system, DM, X, etc. aren't important for a dummy user.
- Concepts: 
    - UNIX philosophy ("Do one thing and do it right") 
    - Modularity (you assemble the components of your operating system), 
    - FOSS $\rightarrow$ support
    - Customizability
    - Efficiency (e.g. do everything with your home row, automate repetitive tasks, get rid of bloat, etc.)
- Comfort:
    - Real power over your machine
    - Example: Pi-hole
    - Example: Some scripts (e.g. streaming series over the terimnal, automating lecture notes, etc. -- *those are my examples which can be found at [my GitLab](https://gitlab.com/alexander_schoch/scripts)*)
- Distros: Different distros mean different foci. This way, everyone can find a distro which fits pretty well for him. I think that it does make sense to show the foci of the most popular distros (Ubuntu, Debian, Arch, Gentoo, Manjaro, OpenSUSE -- *Ubuntu, Mint, Zorin, etc. are pretty much the same, so they don't need to be discussed separately*)
    - Showing the distro tree is IMO a pretty good idea, as it is really impressive. For that, it is not necessary to include it into the slides, as we have seen that you can't really read anything. Just open the .png file and explain a little. Mention e.g. Debian-Ubuntu-Mint, Arch-Manjaro and RedHat-Fedora (because ETH relies on Fedora).
    - Explain the principle of forking (*"Debian was one of the first Linux Distribtions. One day, people thought that Debian is great, but it would be nice if it was a bit more beginner friendly, so they took the source code of Debian, changed it and made a new Distro out of it: Ubuntu. After, some other guys took the source code of Ubuntu and created Linux Mint. Now, we call the parent distributions "upstream" and the child distros "downstream""*).
- Flavours / Desktop Environments / Window Managers: Explain that Windows and Mac have exacly one Desktop (namely "Modern" for Win10 and "Aqua" for MacOS) and you have the choice on Linux. Show different Desktop Environments and explain the difference to a WM. Show e.g. i3wm and Awesomewm. It is also an option to show "pywal" for changing the color scheme according to the wallpaper. Even though this is not a GUI application, it is very easy to use and its effect to newbies is big.
- Show some good GUI applications from the power course in order to show that there are many alternatives to famous Windows/Mac programs (e.g. Krita/GIMP, Audacity, etc.) and provide a link to the full GUI software list from the old toolkit course.

## Table of contents

- 

## Differences to the "old" course

- The licensing slide is pretty much redundant to the FOSS course
- No Windows bashing
- Differences do Linux: What will change for you after switching + Screenshots/Videos?


# Console courses 
<!-- TODO: rename -->


## Console Toolkit - getting started (course + hands-on)

### Content

- 2/3 toolkit + exercises

### Description

After installing some Linux distribution on your computer, it is time to get the best out of it! Learn to use the console by applying the just received input into practice and start becoming a power user.

Please bring your Linux laptop to this course so that you can follow along with the examples.

### Comments on the slides

- slide 17: less is not a file browser but a file reader

## Console Toolkit - going on (course + hands-on)

### Content

- some toolkit (advanced), some power and exercises

### Description

Even though you're now familiar with the use of the console, there is more to discover. This course will guide you on your way to master the command line and will present some really useful terminal applications designed to simplify your workflow.
 
Please bring your Linux laptop to this course so that you can follow along with the examples.
