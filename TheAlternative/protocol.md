# Protokoll

## Anwesend

- Florian (left at IE stuff)
- Nicole
- Alfred
- Jonathan
- Lukas
- Nils
- Alex
- Arthur (left after "A4 Papierkram")
- Dino (after "Distributing tasks")
- Alana (after "Distributing tasks")

Lukas greets.

Update on Marketing Situation: Dino is not here

Update on Room Situation (IE): Found something at ETH somewhere in F floor.

Question of Lukas: No cables ordered, downsizing LAN usage? ergo: we will order the normal set of ports, but won't build anything.

Jonathan proposes to get the rooms much earlier (for GEP). Will be done in Nachbesprechung

Nicole: Uni: no DHCP

## Distributing tasks

Neptun Demodays: already distirbuted. We won't do it on ETH Hoenggerberg. 

Redner Kurse: Beekeeper should work

Helpers for courses should be set earlier (not 20mins before start): 1 dude for multimedia, we will record all courses (exept bash), Jonathan is responsible

- FOSS: Christian Horea, Mo, 1.10.18, 17:15, Alfred, Lukas, Nils, Florian, Alana, Alex
- Intro, Aline Abler, Mi, 3.10.18, 17:15, Nicole, Alfred, Jonathan, Lukas, Nils, Alex
- IE: Everyone
- CT1: Lukas Tobler, Mo, 15.10.18, Florian, Nicole, Alfred, Jonathan, Lukas, Nils, Alex
- CT2: Lukas Tobler, Mi, 17.10.18, Florian, Nicole, Alfred, Jonathan, Lukas, Dino, Alex
- Bash: Aline Abler, Mo, 22.10.18, Alfred, Dino, Lukas, Nils, Alex
- Cubernetes: -, Mi, 24.10.18, Aline, Nicole, Dino, Alfred, Jonathan, Lukas, Alex
- Virtualization: Lenoid, Do, 25.10.18, Nicole, Alfred, Jonathan, Lukas, Nils, Alex

## IE tasks
 
Windows Guy: Florian

- Welcome Desk: Jonathan, Alana as backup, we'll ask others. Providing welcome desk in single room?
- Helper Coordinator: Lukas (1), Nils (2)
- Material Dragon: Amira, Nicole
- Flasher: Dino
- Crowd Control: obsolete
- Construction Coordinator: Nicole
- Catering: Arthur, Chris (ask), Alex, dino has reserved sth in the foyer
- Patrol: Nils, Lukas, Florian and some more. will be set at the day of IE
- Cableguy: obsolete

### Infrastruktur

Lukas.

### Update Cheatsheet (evt. github)

Nils will do sth.

Bumblebee is outdated.

### Install guide changing

- VPN ETH

## Ressorts to distribute

- Catering: Arthur (helferessen), will discuss that later
- Infrasturcture and Equipment: Lukas
- Multimedia: Jonathan
- A4 Papierkram: Nils, (Lukas)
- Videoschnitt: Dino
- Photographing: we will find someone

# Courses

## Feedback

(by florian): He creates a web-application. 


## Slides for Beekeeper

Aline/Nils

# Finances

Dino discussed it with Christan (?) from sca.

per Year: 
- Helferessen 600.-
- IE: 800.-
- Snacks: 300.-
- Ads: 300.-
    - Plakate: 150.-
- Printing: 100.-
- Extra material: 400.-

Income: 1600.- (compicamups)

Last year: approx. 2000.-

Proposal by dino: reduce snacks and extra material

# Varia
## Lecture visits and ads

- 1. sem. inf., bertraud, dino
- 3. sem. inf., roscoe (SPCA), nils
- 1. sem. math/phys --> informatikvorlesung, lukas
- 1. sem. chemie --> huenenberger (alex)
- 3. sem. chemie --> something. (alex)
