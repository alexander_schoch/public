# Anwesend

- Dino
- Lukas
- Nico
- Alana
- Nicole
- Matthias
- Alex

# Begrüssung

Die LinuxDays waren prinzipiell gelungen.

# Rückblick

# Werberessort: 

- Alana: hat funktioniert, analog zu letztem jahr. Leute waren da, hat also gewirkt. Nächstes Semester: werbestandort in UZH weglassen. Prinzipiell viele standorte beibehalten
- Uni-erstibags: Vergessen. Wird nächstes jahr gemacht.
- Activity Fair: War dieses Semester sehr unorganisiert und langweilig.

# Neptun DemoDays 

- War weitläufig sinnlos, im Höngg waren wir nicht. Abschaffen?

## Blogposts

- 1, wurde geschickt. Darf gerne mehr gemacht werden.

# Raumreservation

- Hat dieses Semester nicht gut funktioniert, haben kleine/mühsame Räume erhalten. Nächstes Semester: CAB? HG E3/5? Prinzipiell so früh wie möglich
- TODO: eintrag im wiki: rauminfo.ethz.ch

# Verpflegung

- Last minute, hat aber super funktioniert. Der essensstand wurde wenig promoted und war weit weg. mehr ausschilderung!

# Feedback-System

- Nach linuxdays: Mehr promoten, auf folien

# Kurse

- Hat gut funktioniert
- IE: evt. mehr helfer
- IE: guide für virtualboxes
- evt. IE für chemie-info-studenten
- Spotlight 2 hatte viel zu wenige leute
- Neue Kursaufteilung ist super

# Website

- Ned so cool (zitat: Lukas)
- Rewrite, hat einige Macken und zuwenige infos
- wiki? private pages

# Infrastuktur und Ausstattung

- wiki. Jemand anders muss es machen

# Multimedia

- Jemand muss die Aufnahmen bearbeiten (evt. Lukas). Alex machts, rede mit Sandro (und/oder Jonathan)
- Youtube-passwort: Lukas!

# Sponsoring und Finanzen

- Stammtisch-bier-quittungen sammeln und zurückfordern
- budget passt eigentlich 
- im notfall: apero downgraden

## Helferessen

- Dezember? Januar?
- Woche des 10. Dezember? --> doodle von nicole
- Ort: Aki
- Raclette? --> Sandro, gefragt von Nicole. Alex organisiert Ofen, falls Sandro nicht kann.
- Einkaufen: Sponti

# Fazit

- Planung zu kurzfristig, wie immer. 


