# Notes for the Course Visit

- "Geschätzte Anwesende, ich bin Alex und stehe heute im Namen von TheAlternative vor Ihnen. Wie Sie bestimmt schon auf Plakaten oder in Ihrem Ersti-Bag gsehen haben, finden ab dem 1. Oktober die LinuxDays statt. 
    - Introduction to FOSS: Philosphische Gründe, FOSS-Konzept + Vorteile, Community, Support
    - Introduction to Linux: Distributionen, Auswahl, super einfach. thealternative, weil linux eine alternative und kein ersatz ist.
    - install events: installation von Linux neben windows/mac, Informatikübungen auf dem eigenen notebook
    - Console Toolkit: Benutzung der Konsole, jede software funktioniert dann ähnlich, effizienz
    - bash scripting: automatisierung des alltags, beispiele: serien schauen, notizen machen, musik herunterladen
- Zu diesen kursen können Sie sich auf www.thealternative.ch anmelden
- Bei Fragen bitte ich Sie, in der Pause nach vorne zu kommen. Allenfalls können Sie sich auf unserer website informieren oder mir auch eine E-mail schreiben. Ich werde die adressen dafür in der pause an die tafel schreiben.
- Vielen Dank auch an Prof. Huenenberger, dass er mir die 4 minuten ermöglicht hat. Nun wünsche Ich Ihnen eine schöne pause und einen tollen zweiten Teil der vorlesung
