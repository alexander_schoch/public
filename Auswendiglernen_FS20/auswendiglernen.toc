\contentsline {section}{\numberline {1}Regelungstechnik}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Bode Plots}{2}{subsection.1.1}%
\contentsline {section}{\numberline {2}SPT}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Difference Equation}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Absorption}{3}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Cross-Current, one Stage}{3}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Cross-Current, multiple Stages}{3}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Counter-Current, multiple Stages}{4}{subsubsection.2.2.3}%
\contentsline {subsection}{\numberline {2.3}Packed Column}{4}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Chemical Absorber}{5}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Rachford-Rice Equation}{5}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}Fenske Method}{5}{subsection.2.6}%
\contentsline {subsection}{\numberline {2.7}Underwood Method}{6}{subsection.2.7}%
\contentsline {section}{\numberline {3}HRE}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Two Film}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Dimensionless Numbers}{8}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Hatta}{8}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Enhancement Factor}{9}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Carberry}{9}{subsubsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}Damköhler}{9}{subsubsection.3.2.4}%
\contentsline {subsubsection}{\numberline {3.2.5}Thiele Modulus}{9}{subsubsection.3.2.5}%
\contentsline {subsubsection}{\numberline {3.2.6}Wheeler-Weisz Modulus}{9}{subsubsection.3.2.6}%
\contentsline {subsubsection}{\numberline {3.2.7}Internal Effectiveness Factor}{10}{subsubsection.3.2.7}%
\contentsline {subsubsection}{\numberline {3.2.8}External Effectiveness Factor}{10}{subsubsection.3.2.8}%
\contentsline {subsubsection}{\numberline {3.2.9}Overall Effectiveness Factor}{10}{subsubsection.3.2.9}%
\contentsline {subsubsection}{\numberline {3.2.10}Internal Prater Number}{10}{subsubsection.3.2.10}%
\contentsline {subsubsection}{\numberline {3.2.11}External Prater Number}{10}{subsubsection.3.2.11}%
\contentsline {subsubsection}{\numberline {3.2.12}Dimensionless Activation Energy}{11}{subsubsection.3.2.12}%
\contentsline {subsubsection}{\numberline {3.2.13}Biot for Mass Trasport}{11}{subsubsection.3.2.13}%
\contentsline {subsubsection}{\numberline {3.2.14}Biot for Heat Transport}{11}{subsubsection.3.2.14}%
\contentsline {subsection}{\numberline {3.3}Criteria}{11}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Isothermal, External MT}{11}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Isothermal, Internal MT}{11}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}Non-Isothermal, External MT/HT}{11}{subsubsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.4}Non-Isothermal, Internal MT/HT}{11}{subsubsection.3.3.4}%
\contentsline {subsection}{\numberline {3.4}Shrinking Core}{11}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Apparent Activation Energy}{12}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Adsorption Isotherms}{12}{subsection.3.6}%
\contentsline {subsubsection}{\numberline {3.6.1}Langmuir}{12}{subsubsection.3.6.1}%
\contentsline {subsubsection}{\numberline {3.6.2}Freundlich}{13}{subsubsection.3.6.2}%
\contentsline {subsubsection}{\numberline {3.6.3}Temkin}{13}{subsubsection.3.6.3}%
\contentsline {subsection}{\numberline {3.7}Selectiviy}{13}{subsection.3.7}%
\contentsline {paragraph}{Reactant Shape}{13}{section*.2}%
\contentsline {paragraph}{Product Shape}{13}{section*.3}%
\contentsline {paragraph}{Product Shape}{13}{section*.4}%
\contentsline {section}{\numberline {4}MMM}{13}{section.4}%
\contentsline {subsection}{\numberline {4.1}Method of Characteristics}{13}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Chromatography Columnn}{13}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Sedimentation}{14}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}PFR}{14}{subsubsection.4.1.3}%
