# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

fig, ax = plt.subplots(1, 2, figsize=(6, 3.1))
ax[0].set_aspect('equal')
ax[1].set_aspect('equal')
#ax2 = fig.add_subplot(2,1,2)

rcrs = np.linspace(0,1,100)

ax[0].plot(1 - rcrs**3, rcrs, color='black', linewidth=1, linestyle='-', label='film diffusion')
ax[0].plot(1 - 3 * rcrs*rcrs + 2 * rcrs * rcrs * rcrs, rcrs, linewidth=1, color='black', label='pore diffusion', linestyle='--')
ax[0].plot(1 - rcrs, rcrs, color='black', linewidth=1, linestyle='-.', label='chemical reaction')
ax[0].plot(1 - rcrs**2, rcrs, color='red', linewidth=1, linestyle=':', label='film diffusion (dec. p. s.)')

ax[1].plot(1 - rcrs, rcrs, linewidth=1, color='black', label='film diffusion')
ax[1].plot(1 - 3 * rcrs**(2/3) + 2 * rcrs, rcrs, color='black', linewidth=1, linestyle='--', label='pore diffusion')
ax[1].plot(1 - rcrs**(1/3), rcrs, color='black', linewidth=1, linestyle='-.', label='chemical reaction')
ax[1].plot(1 - rcrs**(2/3), rcrs, color='red', linewidth=1, linestyle=':', label='film diffusion (dec. p. s.)')

# Axis labels
ax[0].set_xlabel(r'$t / \tau_\text{c}$', fontsize=16)
ax[1].set_xlabel(r'$t / \tau_\text{c}$', fontsize=16)
ax[0].set_ylabel(r'$R_\text{c} / R_\text{s}$', fontsize=16)
ax[1].set_ylabel(r'$1 - X_\text{B}$', fontsize=16)

# Grid
ax[0].grid(color='gray',which='both',linestyle=':',linewidth=0.1)
ax[1].grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
ax[0].legend()
ax[1].legend()
plt.tight_layout()
plt.savefig('shrinkingcore.pdf')
