#!/bin/bash

echo "Bitte Namen der Datei eingeben"
printf "> "
read name

if [[ -f "$name" ]] 
then 
  echo "Lade $name auf GitLab"
else
  echo "Diese Datei existiert nicht" 
  exit 1
fi

echo "Du wirst gleich nach nethz-Login und Passwort gefragt. Es wird bei der Eingabe nicht angezeigt."
git clone https://gitlab.ethz.ch/schochal/info1.git
[[ $? != 0 ]] && exit 1
mkdir -p info1/Vera
cp "$name" "info1/Vera/"
cd info1
git add -A
git commit -m 'add Veras file'
echo "Bitte dein Login erneut eingeben"
git push
[[ $? != 0 ]] && exit 1
cd ..
rm -r info1
echo "Tiptop, das sollte so funktioniert haben. Danke :)"
echo "-Alex"
